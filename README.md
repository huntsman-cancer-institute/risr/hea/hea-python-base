# HEA Python Base

The Health Enterprise Analytics (HEA) Python Base is a docker image containing a python installation on Debian Linux 
that we use as the base image for our python projects. Released images are hosted on https://gitlab.com.

## Version history
* 3.12.9: Python 3.12.9. Apt is now configured to avoid potential bad proxy issues on your network manifesting as 
  checksum errors when apt attempts to download an updated package list.
* 3.12.7: Python 3.12.7.
* 3.11.9: Python 3.11.9.
* 3.11.6a: Python 3.11.6 with newer pip to address https://scout.docker.com/v/CVE-2023-5752.
* 3.11.6: Python 3.11.6.
* 3.10.9: Python 3.10.9.
* 3.10.3: Python 3.10.3.
* 3.8.8: Python 3.8.8.

## Runtime requirements
This project should work with any recent version of docker on any supported platform. However, we test mostly on
Windows 10 and 11 hosts.

## Using the image
The image name is `registry.gitlab.com/huntsman-cancer-institute/risr/hea/hea-python-base`. It is publicly available.