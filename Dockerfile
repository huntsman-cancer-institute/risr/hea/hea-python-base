FROM python:3.12.9-slim-bookworm
ENV TZ=America/Denver

# Configure apt to avoid Hash Sum mismatch
RUN echo "Acquire::http::Pipeline-Depth 0;" > /etc/apt/apt.conf.d/99custom && \
    echo "Acquire::http::No-Cache true;" >> /etc/apt/apt.conf.d/99custom

RUN apt-get update && \
    DEBIAN_FRONTEND="noninteractive" apt-get install --no-install-recommends -y dos2unix  tzdata sudo procps ufw net-tools nmap git build-essential curl && \
    apt-get clean && rm -rf /var/lib/apt/lists/*
RUN python -m pip install --upgrade pip