# Releasing HEA Python Base

Releasing HEA Python Base is a two step process:

## Build the Docker image
Run the following command, replacing `<python-version>` with the full version number of Python that will be installed 
(for example, 3.8.8):
```
docker build -t registry.gitlab.com/huntsman-cancer-institute/risr/hea/hea-python-base:<python-version> -t registry.gitlab.com/huntsman-cancer-institute/risr/hea/hea-python-base:latest .
```

## Push the image to a Docker container registry
We use gitlab.com as our container registry. Run the following commands (you need a personal access token with write
access to the HEA Python Base container registry):
```
docker push registry.gitlab.com/huntsman-cancer-institute/risr/hea/hea-python-base:<python-version>
docker push registry.gitlab.com/huntsman-cancer-institute/risr/hea/hea-python-base:latest
```